<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/login.css" />

  <title>MYTREE</title>
</head>

<body>
<div   class="container">

    <div class="msg">
    </div>
    <h1>Asociación de Amigos de Un Millón de Árboles <h1>
    <h2>User Login </h2>

    <form action="<?php echo site_url(['user','authenticate']);?>" method="POST"  class="form-inline" role="form">
      <div id = "container"class="form-group" >
      <center><?php echo $this->session->flashdata('error');?><center>
      <br>
        <label class="sr-only" for="">Username</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Your username">
      </div>
      <BR>
      </BR>
      <div id = "container"class="form-group">
        <label class="sr-only" for="">Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Your password">
      </div>
      <BR>
      </BR>
      <button type="submit" id = "login" class="btn btn-primary" >Login</button>
      <BR>
      </BR>
      <a class="nav-link active" id="register" href="<?php echo site_url(['user','register']); ?>"> REGISTER </a>
    </form>
</div>

</body>
</html>
