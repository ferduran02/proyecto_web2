<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/user.css" />
    <title>User</title>

</head>

<body>
    <div class="container">
        <div class="msg" id="msg">
        </div>
        <h1> Welcome  <?php echo $this->session->user->fullname ?> </h1>
        <a id="logout"href="<?php echo site_url(['user','login']); ?>">Logout</a>
        <h2> List Tree</h2>
        <br>
        <h3>Number of registered <?php echo $this->db->count_all_results('users')-1; ?> users</h3>
        <h3>Number of registered <?php echo $this->db->count_all_results('trees'); ?> trees</h3>
        <br>
        <table id="tabla" class="table table-light">
            <tbody>
                <tr>
                    <td>Id</td>
                    <td>User</td>
                    <td>Tree Name</td>
                    <td>Specie</td>
                    <td>Height</td>
                    <td>Amount</td>
                    <td>View</td>
                </tr>
                <?php 
    foreach ($user as $user) {
      echo "<tr><td>{$user->id}</td><td>{$user->username}</td><td>{$user->nombre}</td><td>{$user->specie}</td>
      <td>{$user->altura}</td><td>{$user->amount}</td><td> 
      <a id='detalle' href='editar/$user->id'>Detail</a> </tr>";} 
      ?>
            </tbody>
        </table>
    </div>

</body>

</html>