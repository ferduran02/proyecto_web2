<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/user.css" />
    <title>User</title>

</head>

<body>
    <div class="container">
        <div class="msg" id="msg">
        </div>
        <h1> Welcome  <?php echo $this->session->user->fullname ?> </h1>
        <a id="logout"href="<?php echo site_url(['user','listTrees']); ?>">back</a>
        <h2> Update Tree</h2>
        <form  action="<?php echo site_url(['user','mod/'.$tree->id]);?>" id="form1" name="form1" onsubmit="return obtenerNombre()"  method="POST" class="form-inline" role="form">
        <br>
        <div class="input-group">
        <h4> Specie: </h4>
            <select id="id_specie" name="id_specie">
            <?php
              if($specie!==null){
                foreach($specie as $especie){
                    ?>
                <option value="<?php echo $especie->nameSpecie;?>" selected="true"><?php echo $especie->nameSpecie;?></option>
                <?php
                    }
              }else{ ?>
                <option value="<?php echo "1";?>" selected="true"><?php echo "N/A";?></option>
     
                <?php          }
                ?>
             </select>
             <input type="hidden" name="especies" />
            </div>
            <h4> Height: </h4>
            <input style=" font-family: 'Times New Roman', Times, serif;font-size: 20px;margin-left: 430px;" 
             id="height" type="text"  name="height">
            <br>
            <h4> Imagen : </h4>
        <input type="file" class="form-control" id="image" name="image" ></input>
          </div>
    <button id="update" style="color:#000408;
                            font-family: 'Times New Roman', Times, serif;
                            font-size: 20px ;
                            margin-left: 570px;
                            border: #026b19 5px solid;
                            margin-top:50px ;
                            background-color: #026b19;"
    type="submit" class="btn btn-primary"> UPDATE TREE</button>
    
</body>


</html>