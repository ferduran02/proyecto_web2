<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/user.css" />
    <title>User</title>

</head>

<body>
    <div class="container">
        <div class="msg" id="msg">
        </div>
        <h1> Welcome  <?php echo $this->session->user->fullname ?> </h1>

          <a id="logout"href="<?php echo site_url(['user','login']); ?>">Logout</a>
        <h2> Purchase Tree</h2>
        
        <form action="<?php echo site_url(['user','registrarArbol']);?>"   method="POST" class="form-inline" role="form">
       
        <h4>Username:
              <input  id="username" type="text" style="background-color:transparent;" value="<?php echo $this->session->user->username ?>" id="username" name="username">
        </h4> 
            <br>

            <div class="form-group">
                <label class="sr-only" for="">Tree Name</label>
                <input type="text" class="form-control" id="nam" name="nam" placeholder="Tree Name">
            </div>
            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Amount</label>
                <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount to donate" <h5> $ </h5>
            </div>
            <br>
            <br>

            <div class="input-group">
            <select id="id_specie" name="id_specie">
            <?php
              if($specie!==null){
                foreach($specie as $especie){
                    ?>
                <option value="<?php echo $especie->nameSpecie;?>" selected="true"><?php echo $especie->nameSpecie;?></option>
                <?php
                    }
              }else{ ?>
                <option value="<?php echo "1";?>" selected="true"><?php echo "N/A";?></option>

                <?php          }
               
                ?>
             </select>
            </div>
            <br>
            <br>
            <button id="user" type="submit" class="btn btn-primary">GET TREE</button>
        </form>
        <br>
        <br>
        <h4> SEE LIST OF SHOPPING TREE </h4>
        <br>
        <a class="nav-link active" id= "login" href="<?php echo site_url(['user','listTree']); ?>"> LIST TREE </a>
      

</body>

</html>