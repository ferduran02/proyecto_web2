<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="check.css" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/register.css" />
    <title>Check In</title>

</head>

<body>

    <div class="container">
        <div class="msg" id="msg">
        </div>
        <h1>Check In User</h1>
        <br>
        <br>
        <form action="<?php echo site_url(['user','registrar']);?>"   method="POST" class="form-inline" role="form">
        
            <div class="form-group">

                <label class="sr-only" for="">Full Name</label>
                <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name">
            </div>
            <br>
            <br>

            <div class="form-group" id="id">
                <label class="sr-only" for="">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
            </div>
            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
            </div>
            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Password</label>
                <input type="password" class="form-control" id="pasword" name="pasword" placeholder="Password">
            </div>

            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Telephone</label>
                <input class="form-control" id="telephone" name="telephone" placeholder="Telephone">
            </div>
            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Username</label>
                <input class="form-control" id="username" name="username" placeholder="username ">
            </div>
            <br>
            <br>
            <div class="form-group">
                <label class="sr-only" for="">Country</label>
                <input class="form-control" id="country" name="country" placeholder="Country">
                <br>
                <br>
                <input class="btn btn-primary" id="save" value="Save User" name="button" type="submit" />
                <br>
                <br>
                <a class="nav-link active" id= "login" href="<?php echo site_url(['user','login']); ?>"> Login Here </a>
            </div>


        </form>

    </div>

</body>

</html>