<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/user.css" />
    <title>User</title>

</head>

<body>
    <div class="container">
        <div class="msg" id="msg">
        </div>
        <h1> Welcome  <?php echo $this->session->user->fullname ?> </h1>

          <a id="logout"href="<?php echo site_url(['user','create']); ?>">Back</a>
        <h2> List Tree</h2>
        <form  action="<?php echo site_url(['user','listTrees']);?>"  method="POST" class="form-inline" role="form">
        
        <br>
        <br>
        <table id="tabla" class="table table-light">
            <tbody>
                <tr>
                    <td>Id</td>
                    <td>User</td>
                    <td>Tree Name</td>
                    <td>Specie</td>
                    <td>Height</td>
                    <td>Amount</td>
                    <td>View</td>
                </tr>
                <?php 
    foreach ($users as $tree) {
      echo "<tr><td>{$tree->id}</td><td>{$tree->username}</td><td>{$tree->nombre}</td><td>{$tree->specie}</td><td>{$tree->altura}</td><td>{$tree->amount}</td><td> 
      <a href='<?php echo site_url(['user','login']); ?> Photo </a> </td> </tr>";
    }?>
            </tbody>
        </table>
    </div>

</body>

</html>