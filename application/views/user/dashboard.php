<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href=" /proyecto_web2/ccs/dashboard.css" />
  <title>Admin</title>
</head>
<body>

<div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('error');?>
    </div>
    <h1> Welcome Admin <?php echo $this->session->user->fullname ?> </h1>

    <a id="logout"href="<?php echo site_url(['user','login']); ?>">Logout</a>
    
    <h2> Dashboard</h2>

    <h4>Username:
              <input type="text" style="background-color:transparent;" value="<?php echo $this->session->user->username ?>" id="username" name="username">
        </h4> 
    <table id="tabla" class="table table-light">
      <tbody>
      <tbody>
          <tr>
              <td>Id</td>
              <td>User</td>
              <td>Specie</td>
              <td>Amount</td>
              <td>View</td>
          </tr>
    <?php 
    foreach ($users as $tree) {
      echo "<tr><td>{$tree->id}</td><td>{$tree->username}</td><td>{$tree->nombre}</td><td>{$tree->amount}</td><td> 
      <a id='detalle' href='<?php echo site_url(['user','login']); ?>Detail</a> | <a href='<?php echo site_url(['user','login']); ?>Delete </a> </td> </tr>";
    }?>
      </tbody>
    </table>
</body>
</html>
