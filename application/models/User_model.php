<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($username, $password){
      $query = $this->db->get_where('users', array('username' => $username, 'password' => $password));
      if ($query->result()) {
        return $query->result()[0];
      } else {
        return false;
      }
  }


  /**
   *  Get all users from the databas
   *  @param $username  The user's
   *
   */
  public function all($username){
      $query = $this->db->get_where('trees', array('username' => $username));
      return $query->result();
  }

  /**
   *  Get trees in Admin
   */
  public function allTree(){
    $query = $this->db->get('trees');
    return $query->result();
}

/**
   *  Register users in Database
   *  @param $fullname  The fullname of user's
   *  @param $lastname The lastname of user's
   *  @param $tipo  The type of user's
   *  @param $email  The email of user's 
   *  @param $email  The email of user's 
   *  @param $pasword  The pasword of user's 
   *  @param $telephone  The telephone of user's 
   *  @param $username  The username of user's
   *  @param $country  The conuntry of user's  
   */  
  public function add($fullname,$lastname,$tipo,$email,$pasword,$telephone,$username,$country){
    $add = array(
      'fullname' => $fullname,
      'lastname' => $lastname,
      'tipo' => 'friend',
      'telephone' => $telephone,
      'username' => $username, 
      'password' => $pasword,
      'email' => $email,  
      'country' => $country);
  
       $this->db->insert('users',$add);
}

/**
   *  Register trees in Database
   *  @param $username  The username of user's
   *  @param $nam The name of trees
   *  @param $specie  The specie of trees
   *  @param $amount  The amount of trees
   */

public function registrarArbol($username,$nam,$specie,$amount){
  $add = array(
    'username' => $username,
    'nombre' => $nam,
    'specie' => $specie,
    'amount' => $amount);

     $this->db->insert('trees',$add);
}

/**
   *  Load combo box of DB
   *
   */

public function especies(){
  $query = $this->db->get('specie');
  return $query->result();
}

/**
   *  Method to edit the trees
   *  @param $id The id of trees
   *  @param $especie The specie of trees
   *  @param $altura The altura of trees
   */

public function mod($id,$especie,$altura){
  if($modificar=="NULL"){
      $consulta=$this->db->query("SELECT * FROM trees WHERE id=$id");
      return $consulta->result();
  }else{
    $consulta=$this->db->query("UPDATE trees SET specie='$especie', altura='$altura'WHERE id =$id;");
    if($consulta==true){
        return true;
    }else{
        return false;
    }
  }
}

public function getTree($id)
    {
        $query = $this->db->get_where('trees', array('id' => $id));
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }


}

