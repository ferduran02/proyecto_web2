<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Method that loads the login
	 */
	public function login()
	{
		$this->load->view('user/login');
	}

	/**
	 * Method that loads a view
	 * Receive an id to know which tree they are editing
	 */
	public function editar($id)
	{
		if($this->user_model!==null){
			if($datatree['specie'] = $this->user_model->especies()){
				$datatree['tree'] = $this->user_model->getTree($id)[0];
				$datatree['treesQty'] = sizeof($datatree['specie']);
			}else{
				$datatree['specie'] = null;
			}
		}else{
			$datatree['specie'] = null;
		}
		$this->load->view('user/edit',$datatree);
	}

	/**
	 * Method that loads the view to close session
	 */

	public function logout()
	{
		$this->session->sess_destroy();
    	$this->session->set_flashdata('error', 'Inicie sesión nuevamente');
		redirect(site_url(['user','login']));
	}


	/**
	 *  Method that loads the view depending on the user
	 */
	public function dashboard()
	{
		if($this->session->has_userdata('user')){
			$this->load->view('user/listTree');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
  }

  /**
   * Method that loads the view register the user
   */
	public function register()
	{
		$this->load->view('user/register');
	}

	/**
	 * Method that loads the view create tree and the combo
	 */
	public function create()
	{
		if($this->user_model!==null){
			if($datatree['specie'] = $this->user_model->especies()){
             
				$datatree['treesQty'] = sizeof($datatree['specie']);
			}else{
				$datatree['specie'] = null;
			}
		}else{

			$datatree['specie'] = null;
		}
		$this->load->view('user/createTree',$datatree);
	}

	/**
	 * Method that loads the view the trees of the users
	 */

	public function listTrees()
	{
		$data['user'] = $this->user_model->allTree();
		$data['usersQty'] = sizeof($data['user']);
		$this->load->view('user/listTree', $data);
	}

	/**
	 * Method that loads the view the trees of the user that is in session
	 */

	public function listTree()
	{
		// get username
		$user = $this->session->user->username;

		$data['users'] = $this->user_model->all($user);
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('user/list', $data);
	}

	/**
	 * Authenticates a user
	 */
	public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('password');
		$user = $this->input->post('username');

		
		$authUser = $this->user_model->authenticate($user, $pass);
		
		if ($authUser) {

			$this->session->set_userdata('user', $authUser);
			$tipoUser = $this->session->user->tipo;

			if($tipoUser == 'admin'){
			redirect(site_url(['user','listTrees']));
			
			}else{
			redirect(site_url(['user','create']));
			}

		}else{
				$this->session->set_flashdata('error', 'Invalid username or password');
				redirect(site_url(['user','login']));
			}
		}
	
		/**
		 * Method that loads me sends the parameters of registering a user
		 */
	public function registrar()
	{
		if($this -> input->post()){
		$fullname = $this->input->post('full_name');
		$lastname = $this->input->post('last_name');
		$email = $this->input->post('email');
		$tipo = 'user';
		$pasword = $this->input->post('pasword');
		$telephone = $this->input->post('telephone');
		$username = $this->input->post('username');
		$country = $this->input->post('country');

		$this->user_model->add($fullname,$lastname,$tipo,$email,$pasword,$telephone,$username,$country);
	
		$this->session->set_flashdata('success', 'Client successfully registered');
		redirect(base_url('user/login'));
		
		}
}

	/**
	 * Method that loads me sends the parameters to register a tree
	 */
public function registrarArbol()
	{
		if($this -> input->post()){
		$username = $this->input->post('username');
		$nam = $this->input->post('nam');
		$specie = $this->input->post('id_specie');
		$amount = $this->input->post('amount');

		$this->user_model->registrarArbol($username,$nam,$specie,$amount);
	
		$this->session->set_flashdata('success', 'Tree successfully registered');
		redirect(base_url('user/create'));
		
		}
}

/**
 * Method that loads me sends the parameters to register a tree...
 */
public function mod($id){
	if($this -> input->post()){
		$species = $this->input->post('id_specie');
		$height = $this->input->post('height');
		$idUser = $id;
	
		$this->user_model->mod($idUser,$species,$height);
		redirect(base_url('user/listTrees'));

		}
		$mi_imagen = 'imagen';
		$config['upload_path'] = "uploads/";
		$config['file_name'] = "nombre_archivo";
		$config['allowed_types'] = "gif|jpg|jpeg|png";
		$config['max_size'] = "50000";
		$config['max_width'] = "2000";
		$config['max_height'] = "2000";
	
		$this->load->library('upload', $config);
	
		if (!$this->upload->do_upload($mi_imagen)) {
			//*** ocurrio un error
			$data['uploadError'] = $this->upload->display_errors();
			echo $this->upload->display_errors();
			return;
		}
	
		$data['uploadSuccess'] = $this->upload->data();
}
	
}
